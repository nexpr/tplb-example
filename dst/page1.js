const html = require("..\\helper\\html.js")
const h1 = require("..\\helper\\h1.js")
const list = require(".\\list.js")
module.exports = $ => ({ html: html`<div>
	<div>1 + 1 = ${1 + 1}</div>
	<div>TEXT: ${$.html}</div>
	<div>HTML: ${{ html: $.html }}</div>
	<div>${h1("H1 TEXT")}</div>
	<div>${list($.items)}</div>
</div>` })