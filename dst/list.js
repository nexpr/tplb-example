const html = require("..\\helper\\html.js")
const item = require(".\\item.js")
module.exports = $ => ({ html: html`<ul>${$.map(item)}</ul>` })